package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	final SportSocialNetworkUserImpl<User> aDelPiero = new SportSocialNetworkUserImpl<User>("Alessandro", "Del Piero", "adp", 45);
    	final SportSocialNetworkUserImpl<User> rFederer = new SportSocialNetworkUserImpl<User>("Roger", "Federer", "rog", 42);
    	final SportSocialNetworkUserImpl<User> uBolt = new SportSocialNetworkUserImpl<User>("Usain", "Bolt", "bolt", 34);
    	
    	aDelPiero.addSport(Sport.SOCCER);
    	aDelPiero.addSport(Sport.MOTOGP);
    	
    	System.out.println("Del Piero play Soccer: " + aDelPiero.hasSport(Sport.SOCCER));
    	System.out.println("Del Piero play Volley: " + !aDelPiero.hasSport(Sport.VOLLEY));
    	
    	rFederer.addSport(Sport.TENNIS);
    	rFederer.addSport(Sport.BASKET);
    	
    	System.out.println("Federer practices Bike: " + !rFederer.hasSport(Sport.BIKE));
    	System.out.println("Federer likes Basket: " + rFederer.hasSport(Sport.BASKET));
    	

    	uBolt.addSport(Sport.SOCCER);
    	uBolt.addSport(Sport.BASKET);
    	
    	System.out.println("Bolt practices Bike: " + !uBolt.hasSport(Sport.BIKE));
    	System.out.println("Bolt practices Soccer: " + uBolt.hasSport(Sport.SOCCER));

    }

}
