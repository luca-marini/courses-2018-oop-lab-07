package it.unibo.oop.lab.nesting2;
import java.util.Iterator;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;

	public OneListAcceptable(List<T> list) {
		this.list = list;
	}

	@Override
	public Acceptor<T> acceptor() {
		
		Iterator<T> iterator = this.list.iterator();
		
		return new Acceptor<T>(){

			public void accept(T newElement) throws Acceptor.ElementNotAcceptedException {
				try {
					if(newElement == iterator.next()) {
						return;
					}
				}catch(Exception e) {
					System.out.println("No more element to be evaluated");
				}
				throw new Acceptor.ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws Acceptor.EndNotAcceptedException {
				try {
					if(!iterator.hasNext()) {
						return;
					}
				}catch(Exception e) {
					System.out.println("2" + e.getMessage());
				}
				throw new Acceptor.EndNotAcceptedException();
			}
			
		};
	}

}
